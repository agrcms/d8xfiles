#!/bin/bash

ssh -p52225 govagrdev@9pro.ca '
cd ../../web/docroot/html/
pushd sites/default
tar --exclude files/legacy -zcvf ~/files.tgz files
popd
../vendor/drush/drush/drush sql-dump --result-file=~/aafc.sql
cd
tar zcf aafc.sql.tgz ./aafc.sql
'
cd `dirname $0`
scp -P52225 govagrdev@9pro.ca:aafc.sql.tgz protox/
scp -P52225 govagrdev@9pro.ca:files.tgz .
pushd protox && rm -rf files && tar zxf ../files.tgz && popd && rm files.tgz
git add .
git commit -m "Refresh from agr-dev.9pro.ca with fileDump.sh"
git push
