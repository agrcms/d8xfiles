#!/bin/sh

cd `dirname $0`
mkdir -p temp && cd $_ && rm -rf *
wget https://agr-dev.9pro.ca/aafc.sql.txt -O aafc.sql
tar zcvf ../protox/aafc.sql.tgz ./aafc.sql
#wget https://agr-dev.9pro.ca/legacy.tgz.txt
#mv aafc.sql.tgz ../protox/
#rm -rf ../protox/files && mv files ../protox/
cd .. && git add .
git commit -m "refresh"
git push
